/*
** Multicast Test-Tool
**
** Copyright (c) 2007, Reto Buerki <reet (at) codelabs.ch>
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

#include <stdlib.h>
#include <stdio.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <sys/utsname.h>
#include <signal.h>
#include <unistd.h>
#include <string.h>

#include "common.h"

/* version */
#define VERSION	"0.3"

/* multicast addr check macro */
#define ADDR_IS_MULTICAST(a) IN_MULTICAST(htonl(a))

void print_usage(char *name)
{
	printf("** mcast-tool [%s], a multicast test tool\n", VERSION);
	printf("usage: %s [options] <group>\n", name);
	printf("options:\n");
	printf(" -h this usage text\n");
	printf(" -t TTL value to use\n");
	printf(" -p port to use, default is port 5000\n");
	printf(" -l enable loopback of mcast traffic\n");
	printf(" -c number of messages to send\n");
	printf(" -d delay in seconds between messages\n");
	printf("    (default is 2 seconds, use 0 for no delay)\n");
	printf(" -i use interface with given address for sending\n");
	printf(" -s show mcast interfaces\n");
	printf(" -r receive only: don't send messages to group\n");
	exit(EXIT_SUCCESS);
}

int main(int argc, char *argv[])
{
	int c, delay;
	struct sockaddr_in mcast_group;
	struct ip_mreq mreq;
	struct utsname name;
	char *if_addr = NULL;

	u_char ttl = TTL;
	u_int port = DEF_PORT;
	u_int enable_loopback = 0;

	u_int msg_count = 1;
	u_int limited_send = 0;

	/* init stats */
	stats.p_recv = 0;
	stats.p_loop = 0;

	delay=DELAY;

	if (argc < 2)
		print_usage(argv[0]);

	while ((c = getopt(argc, argv, "hslrd:p:c:t:i:")) != EOF)
	{
		switch(c){
			case 'h':
				print_usage(argv[0]);
				break;
			case 'd':
				delay=atoi(optarg);
				break;
			case 'p':
				port=atoi(optarg);
				break;
			case 's':
				show_mc_ifaces();
				exit(EXIT_SUCCESS);
			case 'r':
				rcv_only = 1;
				break;
			case 't':
				ttl=atoi(optarg);
				break;
			case 'l':
				enable_loopback = 1;
				break;
			case 'c':
				msg_count = atoi(optarg);
				limited_send = 1;
				break;
			case 'i':
				if_addr=optarg;
				break;
			default:
				break;
		}
	}
	if (argv[optind] == NULL)
	{
		fprintf(stderr, "please specify at least a multicast address\n");
		exit(EXIT_FAILURE);
	}

	char *mcast_addr = argv[optind];
	memset(&mcast_group, 0, sizeof(mcast_group));

	/* ipv4 address check first */
	if (inet_aton(argv[optind], &mcast_group.sin_addr) == 0)
	{
		fprintf(stderr, "no valid IPv4 addr received: %s\n", mcast_addr);
		exit(EXIT_FAILURE);
	}
	if (mcast_group.sin_addr.s_addr == INADDR_NONE)
	{
		fprintf(stderr, "address error: %s\n", mcast_addr);
		exit(EXIT_FAILURE);
	}
	/* multicast addr check */
	if (!ADDR_IS_MULTICAST(mcast_group.sin_addr.s_addr))
	{
		fprintf(stderr, "no valid mcast address: %s\n", mcast_addr);
		exit(EXIT_FAILURE);
	}

	mcast_group.sin_family = AF_INET;
	mcast_group.sin_port = htons(port);

	if (!rcv_only)
	{
		/* create sending UDP socket */
		if ((s_socket = socket(AF_INET, SOCK_DGRAM, 0)) < 0)
		{
			perror("sending socket");
			exit(EXIT_FAILURE);
		}
		/* set TTL option */
		if (setsockopt(s_socket, IPPROTO_IP, IP_MULTICAST_TTL, &ttl,
					sizeof(ttl)) < 0)
		{
			perror("setsockopt(): ttl");
			exit(EXIT_FAILURE);
		}
		/* enable loopback if requested */
		if (!enable_loopback)
		{
			if (setsockopt(s_socket, IPPROTO_IP, IP_MULTICAST_LOOP,
						&enable_loopback, sizeof(enable_loopback)) < 0)
			{
				perror("setsockopt(): loopback");
				exit(EXIT_FAILURE);
			}
		}
	}
	/* create receiving socket and set re-usable */
	if ((r_socket=socket(AF_INET, SOCK_DGRAM, 0)) < 0)
	{
		perror("receiving socket");
		exit(EXIT_FAILURE);
	}
	u_int yes = 1;
	if (setsockopt(r_socket, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(yes)) < 0)
	{
		perror("setsockopt(): reuseaddr");
		exit(EXIT_FAILURE);
	}
	/* bind socket to group address */
	if (bind(r_socket, (struct sockaddr*)&mcast_group,
				sizeof(mcast_group)) < 0)
	{
		perror("bind");
		exit(EXIT_FAILURE);
	}
	/* interface to use */
	if (if_addr == NULL)
	{
		/* let the kernel decide */
		mreq.imr_interface.s_addr = htonl(INADDR_ANY);
	} else {
		mreq.imr_interface.s_addr = inet_addr(if_addr);
		if (setsockopt(s_socket, IPPROTO_IP,
					IP_MULTICAST_IF, &mreq.imr_interface.s_addr,
					sizeof(mreq.imr_interface)) == -1)
		{
			perror("setsockopt(): set mcast if");
			exit(EXIT_FAILURE);
		}
		printf("[send]: using interface with address %s\n", if_addr);
	}

	/* finally join the group */
	mreq.imr_multiaddr = mcast_group.sin_addr;

	if (setsockopt(r_socket, IPPROTO_IP, IP_ADD_MEMBERSHIP,
				&mreq, sizeof(mreq)) < 0)
	{
		perror("setsockopt: add membership");
		exit(EXIT_FAILURE);
	}
	/* read hostname */
	if (uname(&name) < 0)
	{
		perror("uname");
		exit(EXIT_FAILURE);
	}

	int pid = fork();
	switch(pid)
	{
		case -1:
			perror("fork");
			exit(EXIT_FAILURE);
		case 0: { /* child process -> receiving */
			int n;
			u_int len;
			struct sockaddr_in from;
			char msg [MAXLEN + 1];
			char dub[8] = "(LOOP!)\0";
			char addr[INET_ADDRSTRLEN+1];
			char *timestamp;

			signal(SIGINT, sig_handler);
			printf("[recv]: ready\n");
			printf("[recv]: joined group(s): %s\n", inet_ntoa(mcast_group.sin_addr));

			for(;;)
			{
				len = sizeof(from);
				if ((n = recvfrom(r_socket, msg, MAXLEN,
								0, (struct sockaddr*)&from, &len)) < 0)
				{
					perror("recvfrom");
					exit(EXIT_FAILURE);
				}
				msg[n] = 0;
				strncpy(addr, inet_ntoa(from.sin_addr), sizeof(addr));
				timestamp = get_timestamp();
				printf("%s: MSG from %s -> ", timestamp, addr);
				free(timestamp);
				/* check if loopback traffic */
				if(strstr(msg, name.nodename) != NULL)
				{
					stats.p_loop++;
					printf("%s", dub);
				}
				printf("%s\n", msg);
				if (is_new_host(addr))
					add_sending_host(addr, INET6_ADDRSTRLEN);
				stats.p_recv++;

			}
		}
		default: /* parent -> sending */
			if (!rcv_only)
			{
				char msg[MAXLEN];
				printf("[send]: ready\n");
				printf("[send]: (port=%d, TTL=%d, loopback=%c, interval=%d (s))\n", port, ttl,
						(enable_loopback==1 ? 'y':'n'),
						delay);
				printf("[send]: sending...\n");
				snprintf(msg, sizeof(msg), "Hostname: %s, TTL: %d",
						name.nodename, ttl);
				while(msg_count)
				{
					if (sendto(s_socket, msg, strlen(msg), 0,
								(struct sockaddr*)&mcast_group,
								sizeof(mcast_group)) < (ssize_t)strlen(msg))
					{
						perror("sendto");
						terminate_child(pid);
						exit(EXIT_FAILURE);
					}
					sleep(delay);
					if (limited_send)
						msg_count--;
				}
			}
			else
			{
				/* we are just sleeping ... */
				for (;;) { sleep(DELAY); }
			}
			terminate_child(pid);
	}
	exit(EXIT_SUCCESS);
}
