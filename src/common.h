/*
** Multicast Test-Tools
**
** Copyright (c) 2007, Reto Buerki <reet (at) codelabs.ch>
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

#ifndef COMMON_H_
#define COMMON_H_

/* defaults */
#define MAXLEN		1024
#define DELAY		2		/* default delay */
#define DEF_PORT	5000	/* default port */
#define MAX_HOSTS	1000	/* max hosts to store */
#define TTL	  		1		/* default TTL value */

int s_socket, r_socket;		/* sending and recv sockets */
int rcv_only;               /* receiver only switch */

/* struct for statistics */
struct mcast_stats {
		u_int p_recv;	/* packets reveived */
		u_int p_loop;	/* local loopback packets */
		u_int h_len;	/* number of stored hosts */
		char *s_hosts[MAX_HOSTS];	/* sending hosts */
};
static struct mcast_stats stats;

void show_mc_ifaces(void);
void sig_handler(int);
u_int add_sending_host(char*, u_int);
u_int is_new_host(char*);
char* get_timestamp(void);
void free_stats(void);
void terminate_child(int);

#endif
