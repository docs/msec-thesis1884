/*
** Multicast Test-Tools
**
** Copyright (c) 2007, Reto Buerki <reet (at) codelabs.ch>
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <signal.h>
#include <net/if.h>
#include <sys/ioctl.h>
#include <sys/wait.h>
#include <arpa/inet.h>
#include <string.h>
#include <unistd.h>
#include <time.h>

#include "common.h"

void terminate_child(int pid)
{
	kill(pid, SIGTERM);
	int status;
	waitpid(pid, &status, 0);
}

void show_mc_ifaces()
{
	int s,i;
	struct ifconf ifc;
	char inbuf[8192];
	struct ifreq ifr, *ifrp;

	if ((s = socket(AF_INET, SOCK_DGRAM, 0)) < 0)
	{
		perror("socket");
		exit(EXIT_FAILURE);
	}

	ifc.ifc_len = sizeof inbuf;
	ifc.ifc_buf = inbuf;
	if (ioctl(s, SIOCGIFCONF, &ifc) < 0)
	{
		perror("ioctl: SIOCGICONF");
		exit(EXIT_FAILURE);
	}

	ifrp = ifc.ifc_req;
	for ( i = ifc.ifc_len / sizeof(ifr); i > 0; i--, ifrp++)
	{
		strncpy(ifr.ifr_name, ifrp->ifr_name, sizeof(ifr.ifr_name));
		/* get interface flags to see if it's multicast capable */
		if (ioctl(s, SIOCGIFFLAGS, &ifr) < 0)
		{
			perror("ioctl: SIOCGIFFLAGS");
			exit(EXIT_FAILURE);
		}
		if (ifr.ifr_flags & IFF_MULTICAST)
		{
			printf("Interface <%s> has IFF_MULTICAST flag set\n", ifr.ifr_name);
		}
	}
	close(s);
}

void sig_handler(int sig)
{
	u_int i;

	switch(sig)
	{
		case SIGINT:
			printf("\n-----[stats]-------------------------\n");
			printf(" packets received: %d\n", stats.p_recv);
			printf(" packets LOOP    : %d\n", stats.p_loop);
			printf(" hosts in group  : %d\n", stats.h_len);
			for (i = 0; i < stats.h_len; i++)
				printf("\t%s\n", stats.s_hosts[i]);

			/* free memory */
			free_stats();
			/* close sockets */
			if (!rcv_only) {
				close(s_socket);
			}
			close(r_socket);
			exit(EXIT_SUCCESS);
		default:
			break;
	}
}

u_int add_sending_host(char *addr, u_int addrlen)
{
	char *p;
	p = malloc(addrlen);
	strncpy(p, addr, addrlen);
	stats.s_hosts[stats.h_len++] = p;

	return stats.h_len;
}

u_int is_new_host(char *addr)
{
	u_int i;
	for (i = 0; i < stats.h_len; i++)
	{
		if (strcmp(stats.s_hosts[i], addr) == 0)
		{
			return 0;
		}
	}
	return 1;
}

void free_stats()
{
	u_int i;
	for (i = 0; i < stats.h_len; i++)
	{
		free(stats.s_hosts[i]);
	}
}

char* get_timestamp()
{
	time_t tm;
	struct tm tm_time;
	char   *t_buff;

	time(&tm);

	memcpy(&tm_time, localtime(&tm), sizeof(tm_time));

	t_buff = malloc(200);
	strftime(t_buff, 200, "%d-%m-%Y %H:%M:%S", &tm_time);

	return t_buff;
}
