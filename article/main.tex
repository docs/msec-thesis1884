\documentclass[a4paper,twoside,twocolumn,11pt]{article}
\title{Sichere Multicast-Nutzung unter Linux, Kernel 2.6}
\usepackage[ngerman]{babel}
\usepackage{graphicx}
\usepackage[latin1]{inputenc}
\usepackage{listings}
\usepackage[pdftex,colorlinks=true]{hyperref}
\pagestyle{headings}

\author{Reto Buerki}
% language def for bash
\lstdefinelanguage{bash}{%
  moRekeywords={%
                for, if, else, then, fi, echo, case,%
              elif, esac, for, do, done, tar, while, until, shift,%
          mv, mkdir ;;, \$\#, \$1, \$2},%
        sensitive=t, %
  morecomment=[l]{\#} ,%
  morestring=[d]{"}%
}
% Settings for Listings
\lstset{
float=hbp,
language=C,
basicstyle=\ttfamily\small,
identifierstyle=\color{colIdentifier},
keywordstyle=\color{colKeys},
stringstyle=\color{colString},
commentstyle=\color{colComments},
columns=flexible,
tabsize=2,
extendedchars=true,
showspaces=false,
showstringspaces=false,
breaklines=true,
backgroundcolor=\color{hellgelb},
breakautoindent=true,
captionpos=b,
aboveskip=0.8cm}
\begin{document}
\definecolor{hellgelb}{rgb}{1,1,0.8}
\definecolor{colKeys}{rgb}{0,0,1}
\definecolor{colIdentifier}{rgb}{0,0,0}
\definecolor{colComments}{rgb}{1,0,0}
\definecolor{colString}{rgb}{0,0.5,0}

\maketitle
\textbf{Multicast ist die ideale Kommunikationsform f�r breitbandige Anwendungen wie Video-Streaming, Pay-per-View, Video-on-Demand oder Videokonferenzen. Solche Anwendungen verlangen meistens nach einer M�glichkeit der Absicherung der versendeten Daten, sei dies durch die Implementation einer Zugangskontrolle oder auch die Garantie der Vertraulichkeit durch Verschl�sselung. Dieser Artikel beschreibt nach einer Einf�hrung in die Problembereiche von sicheren Multicast-Architekturen den derzeitigen Stand einer IPsec-basierten Implementierung unter Linux.}
\section{Einf�hrung}
TV-Applikationen via Satellit, Software Updates oder ein Informations-Service �ber momentane Aktienkurse sind alles m�gliche Anwendungsszenarien von Multicast. Die Verwendung der Multicast-Technologie reduziert die Last auf Senderseite, Netzwerkelemente wie Multicast-f�hige Router kopieren die einzelnen Pakete und leiten sie zu den Teilnehmern des Services weiter. Der Sender braucht also nur ein Paket zu senden, was den Ressourcenverbrauch bei vielen tausenden Empf�ngern im Vergleich zu Unicast erheblich senkt.

Obwohl Multicast schon seit vielen Jahren erforscht wird, ist der Einsatz in der Praxis immer noch sehr beschr�nkt. Ein Grund daf�r d�rfte sicher das Fehlen von einsatzbereiten Sicherheitsl�sungen im Multicast-Umfeld sein. Sicheres Multicast ist also ein Problem, das f�r die erfolgreiche Verbreitung von Multicast gel�st werden muss. Ein Teilnehmer eines Services f�r B�rseninformationen m�chte sicher sein, dass die empfangene Information authentisch ist, d.h. auch wirklich vom angegebenen Server kommt. Ein Provider von digitalen TV-Programmen m�chte sicherstellen, dass auch wirklich nur zahlende Teilnehmer seinen Service nutzen, anderen bleibt der Zugang zu diesen Programmen verwehrt. Bei Online-Videokonferenzen spielt der Aspekt der Vertraulichkeit eine wichtige Rolle, die versendeten Daten sollten also wenn m�glich verschl�sselt werden. Multicast-Applikationen fordern also die Implementierbarkeit von:
\begin{itemize}
\item Authentifikation und Integrit�t
\item Zugangskontrolle
\item Vertraulichkeit
\end{itemize}

IP-Multicast skaliert sehr gut auf Grund seines offenen Modells: Ein Sender kann Daten zu einer Multicast-Gruppe senden und muss daf�r nicht einmal zwingend ein Member dieser Gruppe sein. Teilnehmer k�nnen Daten einer Gruppe empfangen, indem sie dieser Gruppe beitreten. Dieses offene Modell macht die Entwicklung von sicherem Multicast so extrem schwierig, da die gute Skalierbarkeit nicht auf Kosten der Sicherheit verloren gehen darf.
\begin{figure*}[ht]
\caption{\label{fig:problem-scope}Problembereiche Multicast-Security}
\begin{center}
\includegraphics[scale=0.16]{img/problem-scope-of-msec.png}
\end{center}
\end{figure*}
Sicheres Multicast hat viele Aspekte, die ber�cksichtigt werden m�ssen. Zum einen muss der Sender von Daten diese verschl�sseln und f�r die sp�tere Ur\-sprungs-Au\-then\-ti\-fi\-ka\-ti\-on durch die Empf�nger transformieren. Zur Verschl�sselung wird ein gemeinsamer Schl�ssel verwendet (\emph{Group Key}), der allen Teilnehmern bekannt ist. Auch die Zugangskontrolle kann �ber einen speziellen Schl�ssel durchgef�hrt werden, der nur den berechtigten Hosts bekannt ist. Falls sich die Gruppenmitgliedschaft ver�ndert, m�ssen eventuell Schl�ssel ausgetauscht werden, um dem ausgetretenen Mitglied den Zugang zu zuk�nftigen �bertragungen zu verunm�glichen oder einem neuen Host den Zugang zu den Daten zu erlauben. Dieser Vorgang wird als \emph{Rekeying} bezeichnet.
Es braucht also ein System, dass sich um die Verteilung der verwendeten Schl�ssel k�mmert und die Zugangskontrolle durchf�hrt. Nur berechtigte Teilnehmer erhalten den Gruppenschl�ssel, der f�r die Dechiffrierung der Daten notwendig ist. Diese Funktionen werden in sicheren Multicast-Gruppen im Allgemeinen durch einen GCKS\footnote{GCKS - Group Controller and Key Server} implementiert. Weiter ist wichtig, dass Empf�nger von Daten sicher sein wollen, dass diese Daten auch vom angegebenen Host gesendet wurden, vom Empf�nger muss also eine Authentifikation durchgef�hrt werden k�nnen (\emph{Data Origin Authentification}). Es existieren dabei zwei Formen der Ursprungs-Authentifikation:
\begin{enumerate}
\item \emph{Senderauthentifikation und Datenintegrit�t}\\
       Diese Art der Authentifikation der Daten garantiert, dass sie auch wirklich vom angegebenen Sender versendet und auf dem Weg nicht ver�ndert wurden. Diese Art der Ursprungs-Authentifikation kann in einer Gruppe zum Beispiel durch den Einsatz von digitalen Signaturen erreicht werden.
\item \emph{Gruppenauthentifikation}\\
      Diese Form der Authentifikation garantiert nur, dass die Daten von einem Mitglied der Gruppe generiert oder zuletzt ver�ndert wurden. Datenintegrit�t kann nicht gew�hrleistet werden, ausser alle Gruppenmitglieder sind vertrauensw�rdig. Der Vorteil dieser Variante besteht darin, dass sie relativ einfach umgesetzt werden kann (zum Beispiel mittels eines mit dem Gruppenschl�ssel erzeugten MAC).
\end{enumerate}

Die Abbildung \ref{fig:problem-scope} zeigt die verschiedenen Problembereiche bei der Implementation einer sicheren Multicast-Architektur schematisch. Bei der nachfolgenden Diskussion wird der Fokus auf die Bereiche Schl�sselmanagement, Zugangskontrolle und Datensicherheit reduziert, da sich viele dort gemachten �berlegungen auch auf andere Bereiche �bertragen lassen.

\section{Implementation}
Im Rahmen einer Machbarkeitsstudie wird in diesem Artikel untersucht, inwieweit sich eine sichere Multicast-Architektur bereits heute unter Linux realisieren l�sst. Die Datensicherheit wird dabei von IPsec zur Verf�gung gestellt. Der von Cisco Inc. als Referenz-Implementation entwickelte \texttt{gdoid}-Daemon\footnote{gdoid-Download - \href{http://www.vovida.org/protocols/downloads/gdoi/}{http://www.vovida.org/protocols/downloads/gdoi/}} wird die Zugangskontrolle und die Verteilung des Guppenschl�ssels �bernehmen. Damit dies unter Linux funktioniert, muss der Daemon mindestens in der Version 1.3 vorliegen. F�r die Veranschaulichung dient das in Abbildung \ref{fig:example-net} gezeigte Netzwerk. Die Member Host-A und Host-B sollen sicher �ber die Multicast-Gruppenadresse \textit{239.1.1.1} kommunizieren k�nnen.
\begin{figure}[ht]
\caption{\label{fig:example-net}Beispielnetzwerk}
\begin{center}
\includegraphics[scale=0.15]{img/prototype-net.png}
\end{center}
\end{figure}
\subsubsection{Schl�sselaustausch}
Wenn mit IPsec verschl�sselte Verbindungen aufgebaut werden sollen, so existieren grunds�tzlich zwei verschiedene M�glichkeiten zur Vorgehensweise. Diese unterscheiden sich in dem Austausch der zu verwendenden Schl�ssel. F�r die Authentifizierung und die Verschl�sselung der Pakete werden im gezeigten Beispiel symmetrische Verfahren eingesetzt.
Daher ben�tigen beide Kommunikationspartner die identischen Schl�ssel. Entweder
der Austausch dieser Schl�ssel erfolgt von Hand und man spricht von einer
manuell verschl�sselten Verbindung, oder der Austausch erfolgt automatisch.

Hierf�r wurde das \emph{Internet Key Exchange} (IKE \cite{IKE}) Pro\-to\-koll entwickelt. Dieses Pro\-to\-koll erm�glicht nach Authentifizierung des Partners einen sicheren Schl�sselaustausch nach Diffie- und Hellmann \cite{diffie}. Hierbei k�nnen die Schl�ssel �ber einen unverschl�sselten Kanal so ausgetauscht werden, dass eine dritte Person keinerlei Zugang erh�lt. Das IKE-Pro\-to\-koll sorgt anschliessend f�r einen regelm�ssigen Wechsel dieser Schl�ssel, um ihre Sicherheit zu gew�hrleisten.

Der Linux Kernel ist nicht in der Lage, das IKE-Pro\-to\-koll zu verwenden. Hierf�r
ist ein Userspace-Daemon verantwortlich. Im Multicast-Umfeld kann zu diesem Zweck der bereits erw�hnte \texttt{gdoid}-Daemon verwendet werden, der das von der MSEC Working Group\footnote{MSEC Working Group der IETF - \href{http://www.securemulticast.org/}{http://www.securemulticast.org/}} vorgeschlagene GDOI-Protokoll f�r das Schl�sselmanagement in Gruppen implementiert.

\section{GDOI}
Die GDOI \cite{GDOI} von ISAKMP repr�sentiert einen Versuch der Spezifikation einer ''Domain of Interpretation'' als Erweiterung f�r das generelle ISAKMP Schl�sselmanagement-Framework \cite{ISAKMP}. GDOI ist im Wesentlichen ein GSA \cite{GSA} Management-Pro\-to\-koll, es ist also f�r die Verwaltung der innerhalb von sicheren Gruppen verwendeten SAs\footnote{SA - Security Association} zust�ndig. Die Bezeichnung von ''Domains of Interpretation'' wurde in ISAKMP eingef�hrt, um einen spezifischeren Gebrauch des Pro\-to\-kolls in speziellen Anwendungsszenarien zu erlauben. DOIs erlauben Erweiterungen des ISAKMP-Pro\-to\-kolls, um speziellen Anforderungen zu gen�gen. Genauer ausgedr�ckt nutzt das GDOI-Pro\-to\-koll Definitionen aus GSAKMP\footnote{GSAKMP - Group Secure Association Key Management Protocol, ein anderes Schl�sselmanagement-Protokoll f�r Multicast-Gruppen} \cite{GSAKMP}, bedient sich der Phase 1 SA des Internet DOI \cite{DOI1, IKE} und definiert eigene Payloads und Meldungs-Exchanges nach ISAKMP- und IKE-Standards.

F�r ein Verst�ndnis von GDOI ist es sinnvoll, die 2 Phasen von IKE kurz zu erl�utern. GDOI benutzt zu einem grossen Teil die IKE Phase 1. IKE \cite{IKE} ist in zwei Exchanges unterteilt, die Phase 1 und Phase 2 genannt werden. Ein Phase 1 Exchange muss erfolgreich abgeschlossen worden sein, damit ein Phase 2 Exchange stattfinden kann. Ist ein Phase 1 Exchange erfolgreich, k�nnen sehr viele Phase 2 Exchanges simultan zwischen IKE-Peers ablaufen.
\begin{itemize}
\item \emph{Phase 1}\index{Phase 1}: In der Phase 1 etablieren zwei Peers eine sichere, bidirektionale und authentifizierte Verbindung durch die Verwendung von Payloads und Einheiten, die in ISAKMP definiert sind.

Die beiden Peers verhandeln �ber die anwendbaren kryptographischen Policies und tauschen untereinander Schl�s\-selma\-te\-ri\-al unter Verwendung von Diffie-Hellmann oder Public-Key Verschl�sselungs-Al\-go\-rith\-men aus. Am Ende einer Phase 1 haben sich die beiden Peers erfolgreich Authentifiziert und das n�tige Schl�s\-selma\-te\-ri\-al f�r den Aufbau eines sicheren Kanals f�r die Phase 2 ausgetauscht.
\item \emph{Phase 2}\index{Phase 2}: In der Phase 2 verhandeln die zwei Peers SAs f�r ein Si\-cher\-heits-Pro\-to\-koll wie IPsec. Das in der Phase 1 ausgetauschte Schl�s\-selma\-te\-ri\-al bietet dabei die n�tige Vertraulichkeit, Integrit�t und Re\-play-Schutz.
\end{itemize}
Der in der IKE Phase 1 definierte sichere Kanal wird von GDOI f�r den Schutz des GDOI Schl�s\-selma\-te\-ri\-als verwendet. Die Phase 1 garantiert dabei Vertraulichkeit und Integrit�t. Zudem sch�tzen die IKE-Exchanges vor Man-in-the-Middle Attacken, Connection Hijacking, Reflektion von Daten und Replay-Attacken.

Obwohl GDOI die IKE Phase 1 benutzt, definiert das Pro\-to\-koll einen neuen Phase 2 Exchange, der GROUPKEY\_PULL\index{GROUPKEY\_PULL} genannt wird. GDOI nutzt den IKE Phase 1 Exchange, um diesen neuen Phase 2 Exchange zu sch�tzen. GROUPKEY\_PULL Exchanges werden von den Membern initiiert, um das n�tige Schl�sselmaterial f�r die Etablierung einer SA vom GCKS zu beziehen.

Der GCKS hat zudem die M�glichkeit, ein Update vorhandener SAs oder die Erzeugung neuer SAs auf den Membern zu veranlassen. Diese Meldung wird als GROUPKEY\_PUSH-Meldung bezeichnet und wird durch den KEK oder ein Array von KEKs gesch�tzt. GROUPKEY\_PUSH-Mel\-dungen k�nnen sowohl mittels Unicast als auch via Multicast �bertragen werden.

\subsection{Installation}
Die folgenden Arbeiten sollten alle als root-User ausgef�hrt werden, da der gdoid-Daemon verschiedene Rechte auf speziellen Files �berpr�ft und sich standardm�ssig auf dem Port 848 f�r den Empfang von IKE Phase 1 Exchanges binden will. Auf den Membern ist es zudem n�tig, dass der Daemon in der Lage ist, den Kernel zur Aufnahme von neuen SAs in der SAD\footnote{SAD - Security Association Database. In dieser Datenbank werden SAs abgelegt. Der Linux-Kernel implementiert seine eigene SAD, um die meistens IPsec spezifischen SAs zu verwalten.} zu veranlassen. Diese Funktionalit�t ist verst�ndlicherweise nur privilegierten Prozessen gestattet.

Als erstes sollte das Package an einem geeigneten Ort f�r das Testing entpackt werden:
\begin{verbatim}
  # tar xfvz gdoid-1.3a.tar.gz
\end{verbatim}
Nach dem Entpacken wird der Daemon f�r den sp�teren Compiliervorgang vorbereitet. Dabei wird der \texttt{configure}-Parameter \texttt{--enable-debug} benutzt, damit der Daemon verschiedene Meldungen f�r das Debugging aktiviert. Dies macht die sp�tere Fehlersuche einfacher.
\begin{verbatim}
  # cd gdoid-1.3
  # ./configure --enable-debug
\end{verbatim}
Der Daemon ist verh�ltnism�ssig gen�gsam, was die n�tigen installierten Libraries und Tools angeht. Mindestens OpenSSL\footnote{OpenSSL - \href{http://www.openssl.org/}{http://www.openssl.org/}} und ein \texttt{gcc}-Compiler in einer aktuellen Version sollten aber auf dem Host installiert sein. Zeigt das \texttt{configure}-Script keine Fehler an, kann der Daemon durch den Aufruf von \texttt{make} kompiliert werden:
\begin{verbatim}
  # make
\end{verbatim}
L�uft auch das Kompilieren ohne Fehler ab, ist der Daemon bereit f�r die Konfiguration. Der folgende Abschnitt wird die Konfiguration des Daemons im Detail beschreiben.

\subsection{Konfiguration}
Die einfachste Variante der Konfiguration des \texttt{gdoid}-Daemons basiert auf der Authentifikation mittels Pre-Shared Keys, also Schl�ssel die im vornherein auf den einzelnen Hosts vorhanden sein m�ssen. Diese Keys werden in den Konfigurations-Dateien sowohl auf dem Key-Server als auch den Membern festgelegt.

F�r die Tests wird im Unterverzeichnis \emph{gdoid-1.3/samples} ein neues Verzeichnis \emph{msec} angelegt, das die in Abbildung \ref{fig:pre-shared-conf-tree} dargestellte Struktur aufweist.
\begin{figure}[ht]
\caption{\label{fig:pre-shared-conf-tree}Verzeichnisstruktur}
\begin{center}
\includegraphics[scale=0.18]{img/pre-shared-conf.png}
\end{center}
\end{figure}

Die Scripte \texttt{START\_KS}, \texttt{START\_HOST-A} und \texttt{START\_HOST-B} sind f�r den Start des spezifischen \texttt{gdoid}-Daemons auf dem jeweiligen Host oder Key-Server zust�ndig. Diese Scripte lesen die passende Konfiguration f�r den beteiligten Host. Entsprechende Beispiel-Scripte sind bereits im \emph{samples}-Verzeichnis des Daemons vorhanden und k�nnen angepasst werden. Das Script \texttt{START\_KS} wird zum Beispiel den Daemon mit dem Konfigurationsfile \emph{gdoi\_ks.conf} starten.

Die Konfigurations-Files m�ssen dem \texttt{gdoid}-Prozess-Owner (in diesem Fall root) geh�ren und d�rfen nur von diesem gelesen und geschrieben werden k�nnen. Sind diese Bedingungen nicht erf�llt, verweigert der Daemon das Lesen eines solchen Files.

Damit der GCKS die \emph{Rekey}-Nachrichten korrekt verschl�sseln kann, muss zus�tzlich ein RSA-Schl�sselpaar generiert werden. Das folgende Kommando generiert ein RSA-Schl�sselpaar mit 1024 Bits:
\begin{verbatim}
  # openssl genrsa 1024 > rsakeys.pem
\end{verbatim}
Der Daemon erwartet den Schl�ssel im DER-Format, daher muss eine Konvertierung vorgenommen werden. F�r die Testphase wird der generierte Schl�ssel ins entsprechende Verzeichnis des Daemons kopiert:
\begin{verbatim}
  # openssl rsa -in rsakeys.pem \
     -outform DER -out rsakeys.der
  # cp rsakey.der gdoid-1.3/keys
\end{verbatim}
Diese Schritte m�ssen nur auf dem GCKS-Host durchgef�hrt werden, der Schl�ssel wird f�r die Signierung der \emph{Rekey}-Nachrichten verwendet.

\subsubsection*{Konfigurations-Files}~\\
Nachfolgend werden die wichtigsten Elemente innerhalb der Konfigurations-Files besprochen. Die Konfiguration �hnelt sehr der des \texttt{isakmpd}-Daemons. Ein sehr guter Anhaltspunkt bietet deshalb die \texttt{man}-Page von \texttt{isakmpd.conf}. Auch der \texttt{gdoid}-Daemon bringt eine \texttt{man}-Page f�r seine Konfigurations-Files mit: \texttt{man gdoid.conf}. Dazu muss der Daemon allerdings auf dem System installiert werden.

Im Detail wird nur auf das Konfigurations-File des GCKS eingegangen, da die Konfigurations-Dateien der Group-Member identisch aufgebaut sind.

Das Konfigurations-File ist in Sektionen unterteilt, die mit eckigen Klammern gekennzeichnet werden. Innerhalb solcher Sektionen k�nnen so genannte Tag/Value-Paare definiert werden, wobei der Value-Wert wiederum auf eine andere Sektion innerhalb der Konfigurations-Datei verweisen kann.
\begin{lstlisting}[]{}
  [General]
  Retransmits=          5
  Exchange-max-time=    120
  Listen-on=            192.168.0.4
\end{lstlisting}
Die [General]-Sektion definiert wie der Name schon andeutet globale Werte f�r den Daemon. Wichtig ist vor allem das \emph{Listen-on}-Tag. Dieses Tag legt das Interface fest, auf dem der Daemon auf eingehende IKE Phase 1 Exchanges warten soll.
\begin{lstlisting}[]{}
  [Phase 1]
  Default=              GDOI-group-members
\end{lstlisting}
Die Sektion [Phase 1] funktioniert quasi als Multiplexer f�r eingehende IKE Phase 1 Exchanges f�r die Zugangskontrolle. Durch das \emph{Default}-Tag werden alle Anfragen anhand der in der Sektion [GDOI-group-members] gemachten Angaben �berpr�ft. Es w�re auch m�glich, anhand der IP-Adresse zu multiplexen und praktisch f�r jeden Host innerhalb der Gruppe eigene Angaben f�r den Phase 1 Exchange zu machen. Dies bringt aber zwangsl�ufig einen h�heren administrativen Aufwand mit sich, da jeder Host im Konfigurations-File eingetragen werden muss. In Anwendungen mit im voraus bekannten Hosts mag sich dieser Konfigurationsaufwand aber durchaus lohnen, da dadurch die Sicherheit weiter erh�ht wird. Nur Hosts mit eingetragener IP-Adresse in der [Phase 1]-Sektion werden �berhaupt verarbeitet.

\begin{lstlisting}[]{}
  [GDOI-group-members]
  Phase=                1
  Transport=            udp
  Port=                 848
  Configuration=        Default-main-mode
  Authentication=       msecproject
\end{lstlisting}
Der Daemon liest aus dieser Sektion die Einstellungen f�r den IKE Phase 1 Exchange. Der Transport soll via UDP erfolgen, das \emph{Authentication}-Tag definiert den f�r die Authentifikation verwendete Pre-Shared Key. Alle Hosts im Besitz dieses Keys sind berechtigt, der Gruppe beizutreten.

\begin{lstlisting}[]{}
  [Default-main-mode]
  DOI=                  GROUP
  EXCHANGE_TYPE=        ID_PROT
  Transforms=           3DES-SHA

  [3DES-SHA]
  ENCRYPTION_ALGORITHM= 3DES_CBC
  HASH_ALGORITHM=       SHA
  AUTHENTICATION_METHOD=PRE_SHARED
  GROUP_DESCRIPTION=    MODP_1024
  Life=                 LIFE_3600_SECS

  [LIFE_600_SECS]
  LIFE_TYPE=            SECONDS
  LIFE_DURATION=        600,450:720

  [LIFE_3600_SECS]
  LIFE_TYPE=            SECONDS
  LIFE_DURATION=        3600,1800:7200
\end{lstlisting}
Die Sektion [Default-main-mode] definiert die im Phase 1 Exchange zu verwendenden kryptographischen Parameter. Das Tag \emph{DOI} legt fest, dass die f�r Gruppenkommunikation entworfene ''Domain of Interpretation'' von ISAKMP verwendet werden soll.

Die Sektion [3DES-SHA] definiert die im \emph{Main-Mode} verwendeten Transformationen genauer. F�r die Verschl�sselung wird der 3DES Algorithmus im CBC-Modus\footnote{Cipher Block Chaining Mode (CBC) ist eine Betriebsart, in der Blockchiffrierungsalgorithmen arbeiten. Vor dem Verschl�sseln eines Klartextblocks wird dieser erst mit dem im letzten Schritt erzeugten Geheimtextblock per XOR (exklusives Oder) verkn�pft.} verwendet. Die Exchanges werden zus�tzlich zur Verschl�sselung mit einem SHA-MAC gesch�tzt. Als Schl�ssel f�r die Verifikation des MACs und die Verschl�sselung wird der definierte Pre-Shared Key verwendet. Die Lebenszeit des Schl�ssels wird durch das Tag \emph{Life} festgelegt.

Die Sektion [Phase 2] definiert die f�r den GDOI Phase 2 Exchange zu verwendenden Parameter:
\begin{lstlisting}[]{}
  [Phase 2]
  Passive-Connections=  IPsec-group-policy
\end{lstlisting}
Dabei verweist das Tag \emph{Passive-Connections} auf Verbindungen, die akzeptiert werden k�nnen:
\begin{lstlisting}[]{}
  [IPsec-group-policy]
  Phase=                2
  ISAKMP-peer=          GDOI-group-member
  Configuration=        Default-group-mode
  Group-ID=             Group-1
\end{lstlisting}
Die Konfiguration f�r diese Phase 2-Verbindung wird durch die Sektion [Default-group-mode] weiter spezifiziert. Als ID dient die in der Sektion [Group-1] definierte Group-ID.
\begin{lstlisting}[]{}
  [Group-1]
  ID-type=              KEY_ID
  Key-value=            1234

  [Default-group-mode]
  DOI=                  GROUP
  EXCHANGE_TYPE=        PULL_MODE
  SA-KEK=               GROUP1-KEK
  SA-TEKS=              GROUP1-TEK1
\end{lstlisting}
Die \emph{KEY\_ID} f�r die Gruppe wird auf den Wert 1234 gesetzt, dies muss auch in den Konfigurations-Files der Member so definiert sein. Die in der Sektion [Default-group-mode] gemachten Angaben definieren die innerhalb der Gruppe verwendeten TEKs\footnote{TEK - Traffic Encryption Key. Dieser Schl�ssel garantiert die Vertraulichkeit der versendete Daten} und KEKs\footnote{KEK - Key Encryption Key. Dieser Schl�ssel sch�tzt \emph{Rekeying}-Vorg�nge}. KEKs und TEKs werden im Konfigurations-File �hnlich festgelegt. Nachfolgend die Definition eines TEK:
\begin{lstlisting}[]{}
  [GROUP1-TEK1]
  Crypto-protocol=      PROTO_IPSEC_ESP
  Src-ID=               Group-tek1-src
  Dst-ID=               Group-tek1-dst
  SPI=                  287484603
  TEK_Suite=            QM-ESP-3DES-SHA-SUITE-MSEC
  DES_KEY1=             ABCDEFGH
  DES_KEY2=             IJKLMNOP
  DES_KEY3=             QRSTUVWX
  SHA_KEY=              12345678901234567890

  [Group-tek1-src]
  ID-type=              IPV4_ADDR
  Address=              192.168.0.198
  Port=                 1024

  [Group-tek1-dst]
  ID-type=              IPV4_ADDR
  Address=              239.1.1.1
  Port=                 1024

  [QM-ESP-3DES-SHA-SUITE-MSEC]
  Protocols=            QM-ESP-3DES-SHA-MSEC

  [QM-ESP-3DES-SHA-MSEC]
  PROTOCOL_ID=          IPSEC_ESP
  Transforms=           QM-ESP-3DES-SHA-XF-MSEC

  [QM-ESP-3DES-SHA-XF-MSEC]
  TRANSFORM_ID=         3DES
  ENCAPSULATION_MODE=   TRANSPORT
  AUTHENTICATION_ALGORITHM= HMAC_SHA
  Life=                    LIFE_600_SECS
\end{lstlisting}
Das Tag \emph{TEK\_Suite} definiert die f�r den TEK zu verwendenden Transformationen. Als Crypto-Protokoll wird IPsec ESP im Transport Mode verwendet. Dieses nutzt 3DES f�r die Verschl�sselung und einen durch den SHA-Algorithmus erzeugten HMAC f�r die Ursprungs-Authentifikation. Die Lebenszeit einer SA wird auf 600 Sekunden beschr�nkt. Die erste SA wird auf den Membern mit dem Wert 287484603 (\emph{0x1122aabb}) erzeugt. Die Sektion [Group-tek1-src] definiert die Source (den Sender) innerhalb der Multicast-Gruppe mit der Gruppenadresse 239.1.1.1 (Sektion [Group-tek1-dst]. Dabei ist es m�glich, je nach Anzahl sendender Parteien mehrere TEKs zu definieren. In der gezeigten Konfiguration kann nur der Host-A Daten senden. Der Member Host-B kann nur empfangen.

\subsection{Testphase}
Wurde der Daemon erfolgreich kompiliert und die n�tigen Konfigurations-Files f�r die einzelnen Hosts erstellt, kann ein erster Test durchgef�hrt werden.

Auf dem GCKS wird der Daemon durch das entsprechende Script im \emph{Pre-Shared}-Verzeichnis gestartet:
\begin{verbatim}
  # ./START_KS
\end{verbatim}
Der Daemon wartet nun, die korrekte Konfiguration vorausgesetzt, auf der konfigurierten IP-Adresse auf eingehende Phase 1 Exchanges. Fehlermeldungen in Bezug auf x.509-Zertifikate k�nnen ignoriert werden, da in dieser Konfiguration keine Zertifikate f�r die Authentifikation zum Einsatz kommen.

Nun kann auf den beiden Membern der Daemon mit den entsprechenden Scripten \texttt{START\_HOST-A} und \texttt{START\_HOST-B} gestartet werden.
Mit den �bergebenen Parametern werden die Daemons eine grosse Anzahl von Log-Meldungen erzeugen, die wichtigsten Meldungen sind aber die folgenden:
\begin{verbatim}
  Exch 50 gdoi_finalize_exchange: \
    DONE WITH PHASE 1!!!
\end{verbatim}
Diese Meldung zeigt an, dass sich ein Gruppen-Member erfolgreich authentifiziert hat und das n�tige Schl�ssel-Material f�r den GDOI Phase 2 Exchange zum Member heruntergeladen wurde. L�uft alles wie geplant, sollte am Ende die folgende Meldung erscheinen:
\begin{verbatim}
  Exch 50 gdoi_finalize_exchange: \
    DONE WITH PHASE 2!!!
\end{verbatim}
Diese Meldung bedeutet, dass auch der GDOI Phase 2 Exchange (auch GROUPKEY\_PULL genannt) erfolgreich war und der Member nun �ber die n�tigen Schl�ssel (KEK und TEK) verf�gt, um an der Gruppenkommunikation teilzunehmen. Auf einem Member kann dies durch das \texttt{setkey}-Kommando �berpr�ft werden (Ausgabe gek�rzt):
\begin{verbatim}
  # setkey -DP
  192.168.0.198[any] 239.1.1.1[any]
     in prio low + 136394240 ipsec
     esp/transport//require
     created: Sep 20 20:01:04 2005
     lifetime: 0(s) validtime: 0(s)
     spid=208 seq=1 pid=28129
     refcnt=1
  192.168.0.198[any] 239.1.1.1[any]
     out prio low + 136394240 ipsec
     esp/transport//require
     created: Sep 20 20:01:04 2005
     lifetime: 0(s) validtime: 0(s)
     spid=201 seq=0 pid=28129
     refcnt=1
\end{verbatim}
Die Eingabe von \texttt{setkey -DP} zeigt die in der SPD des Kernels registrierten Policies an. Der Host-A verf�gt nun �ber die im Konfigurations-File festgelegte Policy f�r die Multicast-Kommunikation zur Gruppenadresse \emph{239.1.1.1}. F�r den Host-A ist nur die zweite Policy (\emph{out}) von Bedeutung, da er der sendende Host in der Multicast-Gruppe darstellt. Die erste Policy (\emph{in}) ist f�r den Host-B wichtig, damit er von Host-A ankommende Pakete entschl�sseln kann. In der Policy ist zudem ersichtlich, dass alle Protokolle (\emph{any}) durch IPsec ESP im Transport-Modus verschl�sselt werden.

Die in der SAD des Kernels registrierten SAs k�nnen mit dem Aufruf \texttt{setkey -D} angezeigt werden. Zu Beginn ist nur eine SA der Kategorie 3 vorhanden, da bis dahin noch keine \emph{Rekey}-Nachrichten vom GCKS gesendet wurden. Die SA sollte dabei den im Konfigurations-File festgelegten SPI-Wert von \emph{0x1122aabb} aufweisen. Der Status \emph{mature} gibt an, dass die SA g�ltig ist und vom Kernel verwendet werden kann. ~\\
Die Lebensdauer der SA ist wie folgt ersichtlich:
\begin{verbatim}
  diff: 35(s)     hard: 600(s)
  soft: 540(s)
\end{verbatim}
Der \emph{diff}-Wert zeigt an, wie lange die SA schon in der SAD des Kernels eingetragen ist. Erreicht dieser Wert 540, wird der Kernel alle an dieser SA interessierten Prozesse �ber den PF\_KEY-Socket �ber das so genannte SOFT-Timeout dieser SA informieren. Die SA wechselt vom Status \emph{mature} in den \emph{dying}-Status. Der \texttt{gdoid}-Daemon kann dann gegebenenfalls Massnahmen einleiten, wie zum Beispiel den GCSK um neue SAs bitten. Erreicht der \emph{diff}-Wert den im \emph{hard}-Feld ersichtlichen Wert von 600 Sekunden, wird die SA vom Kernel aus seiner SAD gel�scht (HARD-Timeout). Auch dieser Vorgang wird vom Daemon registriert, da der Kernel wiederum eine spezielle Nachricht �ber den \emph{PF\_KEY}-Socket versendet. Diese Werte stimmen mit denen im Konfigurations-File des GCKS �berein (LIFE\_600\_SECS).

Der GCKS sendet in konfigurierbaren Abst�nden Updates f�r vorhandene SAs oder Informationen f�r die Generierung von SAs. Diese lassen sich wiederum mit den entsprechenden \texttt{setkey}-Kommandos anzeigen.

Ob die Daten auch wirklich verschl�sselt �bertragen werden, kann mit einem Aufruf von \texttt{ping} leicht �berpr�ft werden:
\begin{verbatim}
  # ping -c1 239.1.1.1
\end{verbatim}
Dieser Aufruf sendet genau ein Paket an die Multicast-Gruppenadresse. Wird das Kommando zum Beispiel auf dem Host-A ausgef�hrt, sieht der Output von \texttt{tcpdump} wie folgt aus:
\begin{verbatim}
  # tcpdump multicast
    IP 192.168.0.198 > 239.1.1.1:
      ESP(spi=0x1122aabb,seq=0x3d)
\end{verbatim}
Der ausgehende ICPM \emph{Echo-Request} wird durch die ESP SA mit der SPI \emph{0x1122aabb} gesch�tzt. Der Host-B wird auf diese Anfrage antworten, da er das Paket dank einer identischen SA entschl�sseln kann. Die Antwort erfolgt allerdings unverschl�sselt mittels Unicast.

\section{Fazit}
Es ist bereits m�glich, eine sicher Multicast-Architektur auf Linux-Basis zu realisieren. Mit Hilfe von IPsec wird die n�tige Transportsicherheit garantiert, ein Userspace-Daemon wie \texttt{gdoid} �bernimmt die Zugangskontrolle und die Verteilung des Gruppenschl�ssels an die Member. Nat�rlich kann eine solche Architektur nicht alle Anforderungen an die Sicherheit abdecken. Vor allem bei der Existenz von mehreren Sendern in einer Multicast-Gruppe m�ssen noch Vorschl�ge aus den �berarbeiteten IPsec Spezifikationen \cite{MESP, MESP2} implementiert werden, damit das SA-Handling auf den Membern vereinfacht werden kann.

Obwohl es unter Linux bereits m�glich ist, dass mehrere Sender in der Gruppe vorhanden sind, ist im \texttt{gdoid}-Daemon im Moment nur eine Gruppenauthentifikation auf Basis eines MAC realisiert. Senderauthentifikation ist vor allem mit Daten in Echtzeit eine grosse Herausforderung. Es existieren aber bereits entsprechende Protokolle und Vorschl�ge, um auch diese Eigenschaft in einer sicheren Gruppe effizient gew�hrleisten zu k�nnen. Interessierte Leser seien vor allem auf \cite{TESLA} und \cite{rsa-sig-esp} verwiesen.

\section{Ausblick}
Vor allem die Verf�gbarkeit von digitalem TV und den damit verbundenen Applikationen wie VOD\footnote{VOD - Video-on-Demand} oder PPV\footnote{PPV -  Pay-per-View} werden die Entwicklung von sicheren Mul\-ti\-cast-Tech\-no\-lo\-gien in n�chster Zeit enorm f�rdern. Gerade in solchen Anwendungen ist es den Anbietern wichtig, dass keine nicht-berechtigten Nutzer Zugang zu den versendeten Daten erhalten. W�hrend Mul\-ti\-cast eine effiziente Form der Datenkommunikation zwischen den Providern und Usern darstellt, ist ein Framework n�tig, das die �bertragenen Daten sch�tzt. Es darf angenommen werden, dass grosse Anbieter von digitalen TV-Dienst\-leis\-tungen intensiv an L�sungen forschen. Bleibt zu hoffen, dass Teile dieser Entwicklung und Forschung auch �ffentlich zug�nglich gemacht werden und nicht nur als propriet�re Firmenl�sungen Verbreitung finden.

Einige Dokumente wie zum Beispiel GDOI \cite{GDOI} sind bereits auf dem Weg zur Standardisierung. Einflussreiche Firmen wie Cisco f�rdern den Einsatz dieses Protokolls und es wird mit grosser Wahrscheinlichkeit zu einem der Standard-Protokolle f�r das Schl�sselmanagement in sicheren Mul\-ti\-cast-Gruppen avancieren. Die dazugeh�rige Referenz-Im\-ple\-men\-ta\-ti\-on ist nun auch f�r Linux erh�ltlich und funktioniert angesichts der Neuheit des PF\_KEY-Codes innerhalb des Daemons sehr gut. In diesem Artikel wurde beschrieben, wie mit diesem Daemon bereits heute eine sichere Mul\-ti\-cast-Architektur realisiert werden kann.

\section{Autor}
Reto Buerki ist Diplomstudent an der Fachhochschule Solothurn Nordwestschweiz. Seine Diplomarbeit befasst sich mit dem Thema ''Sichere Multicast-Nutzung unter Linux, Kernel 2.6''. Er kann �ber die E-Mail Adresse \emph{$<$reto@buerki.info$>$} erreicht werden.

\begin{thebibliography}{12}

\bibitem{MESP}            Canetti, R., P. Rohatgi, and P. Cheng: \emph{Multicast Data Security Transformations: Requirements, Considerations, and Proposed Design}, draft-irtf-smug-data-transforms-00.txt, IRTF, June 2000, in Arbeit

\bibitem{MESP2}           S. Kent and K. Seo, \emph{Security Architecture for the Internet Protocol}, draft-ietf-ipsec-rfc2401bis-06.txt, IETF, M�rz 2005

\bibitem{GDOI}            Baugher, M., B. Weis, et al., \emph{The Group Domain of Interpretation}, RFC 3547, Juli 2003

\bibitem{GSAKMP}          Harney, H., et al., \emph{GSAKMP: Group Secure Association Key Management Protocol}, draft-ietf-msec-gsakmp-sec-10.txt, IETF, M�rz 2001, in Arbeit

\bibitem{ISAKMP}           Maughan, D., et al., \emph{Internet Security Association and Key Management Protocol (ISAKMP)}, RFC 2408, IETF, November 1998

\bibitem{DOI1}             Piper, D., \emph{The Internet IP Security Domain of interpretation for ISAKMP}, RFC 2407, IETF, November 1998

\bibitem{IKE}              Harkins, D., and D. Carrel, \emph{The Internet Key Exchange (IKE)}, RFC 2409, IETF, November 1998

\bibitem{GSA}              T. Hardjono and B. Weis, \emph{The Multicast Group Security Architecture}, RFC 3740, IETF, M�rz 2004

\bibitem{PF-KEY}           McDonald, D., Metz, C., and B. Phan, \emph{PF\_KEY Key Management API, Version 2}, RFC 2367, IETF, July 1998

\bibitem{IPsec}            S. Kent and R. Atkinson, \emph{Security Architecture for the Internet Protocol}, RFC 2401, IETF, November 1998

\bibitem{diffie}           E. Rescorla, \emph{Diffie-Hellman Key Agreement Method}, RFC 2631, IETF, Juni 1999

\bibitem{LKH}              Wallner, D., E. Harder, and R. Agee, \emph{Key Management for Multicast: Issues and Architectures}, RFC 2627 (informational), IETF, Juni 1999

\bibitem{TESLA}           Perrig, A., et al., \emph{TESLA: Multicast Source Authentication Transform}, draft-irtf-smug-tesla-00.txt, IRTF, November 2000, in Arbeit

\bibitem{rsa-sig-esp}      B. Weis, \emph{The Use of RSA/SHA-1 Signatures within ESP and AH}, IETF, Memo, Juni 2005
\end{thebibliography}
\end{document}
